import './App.css';
import Header from './components/Header';
import VehicleMakeTable from './components/VehicleMakeTable';
import VehicleModelTable from './components/VehicleModelTable';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Footer from './components/Footer';

function App() {
  return (
    <Router>
    <div className='container'>
      <Header />
      <div className="container2">
        <Routes><Route path='/' exact element={(
          <div className='container3'>
            <h3>Welcome to the page of the most awesome cars of 2021!</h3>
            <div className="container4">
              <p>Wow, 2021 flew by and all too soon we were looking back down the long list of new cars we'd tested over the last 12 months, trying to pick out some highlights. Our list of the best cars of 2021 is based on what people like you have to say about their new vehicles after they've owned them for at least 90 days.</p>
              <iframe src="https://giphy.com/embed/3ohs7K8l2xVqyHwfGE" width="200" height="200" frameBorder="0" className="giphyembed"></iframe>
            </div>
          </div>
        )} /></Routes>
        <Routes><Route path='/model' element={ <VehicleModelTable /> } /></Routes>
        <Routes><Route path='/make' element={ <VehicleMakeTable /> } /></Routes> 
      </div>
      <Footer />
    </div>
    </Router>
  );
}

export default App;
