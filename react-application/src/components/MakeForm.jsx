import React from 'react'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { Form } from 'react-bootstrap';
import * as axios from 'axios';
import { useState } from 'react';

const MakeForm = (props) => {
  const { setOpenPopup } = props;

  const url = "http://localhost:5000/vehiclemake";

  const [data, setData] = useState({
    name: "",
    abrv: ""
  })

  function handle(e){
    const newdata = {...data}
    newdata[e.target.id] = e.target.value
    setData(newdata)
  }

  const closePopup = () => {
    setOpenPopup(false)
  }
  
  function submitMake(e){
    e.preventDefault();
    axios.post(url, {
      name: data.name,
      abrv: data.abrv
    }).then(res => {
      console.log(res.data)
      if(res.data !== ""){
        closePopup()
        window.location.reload(false);
      }
    }) 
  }

  return (
    <Form onSubmit={(e)=>submitMake(e)}>
      <TextField
        margin="dense"
        id="name"
        label="Name"
        type="text"
        fullWidth
        variant="standard"
        value={data.name}
        onChange={(e)=>handle(e)}
        required
      ></TextField>
      <TextField
        margin="dense"
        id="abrv"
        label="Abrv"
        type="text"
        fullWidth
        variant="standard"
        onChange={(e)=>handle(e)}
        value={data.abrv}
        required
      ></TextField>
      <div className='flex justify-end'>
        <Button type='submit'>Subscribe</Button>
      </div>
    </Form>
  )
}

export default MakeForm
