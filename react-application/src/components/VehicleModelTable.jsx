import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { useState, useEffect } from 'react';
import { MdDelete } from 'react-icons/md';
import { GridActionsCellItem } from '@mui/x-data-grid';
import { Button } from '@mui/material';
import AddIntoModelTable from './AddIntoModelTable';
import ModelForm from './ModelForm';
import { useObserver } from "mobx-react";

const VehicleModelTable = () => {
  const [tableData, setTableData] = useState([])
  const [openPopup, setOpenPopup] = useState(false);

  useEffect(() => {
    const getCars = async () => {
      const carsFromServer = await fetchCars()
      setTableData(carsFromServer)
    }
    getCars()
  }, [])

  const fetchCar = async (id) => {
    const res = await fetch(`http://localhost:5000/vehiclemodel/${id}`)
    const data = await res.json()

    return data
  }

  const fetchCars = async () => {
    const res = await fetch(`http://localhost:5000/vehiclemodel`)
    const data = await res.json()
    return data
  }

  const updateCellEdit = async (params) => {
    const carToUpd = await fetchCar(params.id)
    const updCar = { ...carToUpd, [params.field]: params.value }
    const res = await fetch(`http://localhost:5000/vehiclemodel/${params.id}`, {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json'
      }, 
      body: JSON.stringify(updCar)
    })

    const data = await res.json()

    setTableData(tableData.map((car) => car.id === params.id ? { ...car, makeid: data.makeid, name: data.name, abrv: data.abrv} : car))
  } 

  const deleteCar = React.useCallback(
    (id) => async () => {
      console.log(id)
      await fetch(`http://localhost:5000/vehiclemodel/${id}`,{ method: 'DELETE',})
     
      setTimeout(() => {
        setTableData((prevRows) => prevRows.filter((row) => row.id !== id));
      });
    },
    [],
  );
  
  const columns = [
    { field: 'id', headerName: 'ID', width: 100 },
    { field: 'makeid', headerName: 'Make ID', width: 100, editable: true },
    { field: 'name', headerName: 'Name', width: 200, editable: true },
    { field: 'abrv', headerName: 'Abrv', width: 150, editable: true },
    {
      field: 'actions',
      type: 'actions',
      getActions: (params) => [
        <GridActionsCellItem icon={<MdDelete />} className="hover:text-red-600" onClick={deleteCar(params.id)} label="Delete" />,
      ]
    }
  ];

  return useObserver (() => (
    <div style={{ height: 400, width: '100%' }}>
      <Button variant="outlined" onClick={() => setOpenPopup(true)}>
        Add a row
      </Button>
      <AddIntoModelTable 
        title = "Add Vehicle Model"
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}>
        <ModelForm />
      </AddIntoModelTable>
      <DataGrid
        rows={tableData}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        onCellEditCommit={updateCellEdit}
      />
    </div>
  ));
}

export default VehicleModelTable