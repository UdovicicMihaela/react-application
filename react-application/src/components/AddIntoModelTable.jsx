import React from 'react'
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Typography } from '@mui/material';
import ModelForm from './ModelForm';

const AddIntoModelTable = (props) => {
    const {title, openPopup, setOpenPopup} = props;

    return( 
      <Dialog open={openPopup}>
          <DialogTitle className='flex justify-between'>
              <Typography variant='h5' component="div">
                { title }
              </Typography>
              <Button onClick={() => {setOpenPopup(false)}}>Cancel</Button>
          </DialogTitle>
          <DialogContent dividers>
            <ModelForm  setOpenPopup={setOpenPopup} />
          </DialogContent>
      </Dialog>    
    ) 
}

export default AddIntoModelTable
