import React from 'react'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { Form } from 'react-bootstrap';
import * as axios from 'axios';
import { useState } from 'react';

const ModelForm = (props) => {
  const { setOpenPopup } = props;

  const url = "http://localhost:5000/vehiclemodel";

  const [data, setData] = useState({
    makeid: "",
    name: "",
    abrv: ""
  })

  function handle(e){
    const newdata = {...data}
    newdata[e.target.id] = e.target.value
    setData(newdata)
  }

  const closePopup = () => {
    setOpenPopup(false)
  }
  
  function submitModel(e){
    e.preventDefault();
    axios.post(url, {
      makeid: data.makeid,
      name: data.name,
      abrv: data.abrv
    }).then(res => {
      console.log(res.data)
      if(res.data !== ""){
        closePopup()
        window.location.reload(false);
      }
    }) 
  }

  return (
    <Form onSubmit={(e)=>submitModel(e)}>
      <TextField
        margin="dense"
        id="makeid"
        label="Make ID"
        type="text"
        fullWidth
        variant="standard"
        value={data.makeid}
        onChange={(e)=>handle(e)}
        required
      ></TextField>
      <TextField
        margin="dense"
        id="name"
        label="Name"
        type="text"
        fullWidth
        variant="standard"
        value={data.name}
        onChange={(e)=>handle(e)}
        required
      ></TextField>
      <TextField
        margin="dense"
        id="abrv"
        label="Abrv"
        type="text"
        fullWidth
        variant="standard"
        onChange={(e)=>handle(e)}
        value={data.abrv}
        required
      ></TextField>
      <div className='flex justify-end'>
        <Button type='submit'>Subscribe</Button>
      </div>
    </Form>
  )
}

export default ModelForm
