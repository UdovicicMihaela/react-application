const Footer = () => {
  return (
    <div className="footer">
      <p>Minimalistic application for <a className="toMono" href="https://www.mono.hr/" target="_blank">Mono</a></p>
    </div>
  )
}

export default Footer
