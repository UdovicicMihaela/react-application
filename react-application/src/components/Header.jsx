import { Navbar, Nav, Container } from "react-bootstrap"

const Header = () => {
  return (
    <div className="header">
      <Navbar bg="light" expand="lg">
        <Container>
            <Navbar.Brand href="/">Best cars of 2021</Navbar.Brand>
            <Nav className="me-auto">
              <Nav.Link href="model">Vehicle Model</Nav.Link>
              <Nav.Link href="make">Vehicle Make</Nav.Link>
            </Nav>
        </Container>
    </Navbar>
    </div>
  )
}

export default Header
